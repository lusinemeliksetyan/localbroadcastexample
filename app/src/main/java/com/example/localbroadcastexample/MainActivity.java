package com.example.localbroadcastexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private LocalBroadcastManager localBroadcastManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.registerReceiver(broadcastReceiver, new IntentFilter("CUSTOM_ACTION"));
        // Create intent with action
        Intent localIntent = new Intent("CUSTOM_ACTION");
        localIntent.putExtra("data", 10);
        // Send local broadcast
        localBroadcastManager.sendBroadcast(localIntent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        localBroadcastManager.unregisterReceiver(broadcastReceiver);
    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Toast.makeText(context, "awdawdawdawda", Toast.LENGTH_SHORT).show();
            if (intent != null && intent.getAction().equals("CUSTOM_ACTION")) {
                if (intent.hasExtra("data")) {
                    int number = intent.getIntExtra("data", -1);
                    Toast.makeText(context, String.valueOf(number), Toast.LENGTH_SHORT).show();
                }
            }
        }
    };
}
